customElements.define('person-details',
  class extends HTMLElement {
    constructor() {
      super();

      const template = document.getElementByClassName('person-template');
      
      const templateContent = template.content;

      const shadowRoot = this.attachShadow({mode: 'open'});

      shadowRoot.appendChild(templateContent.cloneNode(true));
    }
  }
);
